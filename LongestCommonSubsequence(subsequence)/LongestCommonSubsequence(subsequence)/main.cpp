#include <iostream>
#include <vector>

typedef enum
{
    S1 = 1,
    S2,
    S1S2
}pointer_table_enum;

using namespace std;

class Engine
{
    private:
        string str1;
        string str2;
        int    str1Length;
        int    str2Length;
        vector< vector<int> > matrixVector;
        vector< vector<int> > pointerTableVector;
    
        void initializeVectors()
        {
            for(int i = 0 ; i < str1Length ; i++)
            {
                vector<int> innerVector;
                for(int j = 0 ; j < str2Length ; j++)
                {
                    innerVector.push_back(0);
                }
                matrixVector.push_back(innerVector);
                pointerTableVector.push_back(innerVector);
            }
        }
    
        void replaceVectorValue(int i , int j)
        {
            if(str1[i] == str2[j])
            {
                pointerTableVector[i][j] = S1S2;
                if(i && j)
                {
                    matrixVector[i][j] = matrixVector[i-1][j-1] + 1;
                }
                else
                {
                    if(!i && !j)
                    {
                        matrixVector[i][j] = 1;
                    }
                    else if (!i)
                    {
                        matrixVector[i][j] = matrixVector[i][j-1];
                    }
                    else
                    {
                        matrixVector[i][j] = matrixVector[i-1][j];
                    }
                }
            }
            else
            {
                if(i && j)
                {
                    matrixVector[i][j]       = max(matrixVector[i][j-1] , matrixVector[i-1][j]);
                    pointerTableVector[i][j] = matrixVector[i][j-1] > matrixVector[i-1][j] ? S1 : S2;
                }
                else
                {
                    if(!i && !j)
                    {
                        matrixVector[i][j]       = 0;
                        pointerTableVector[i][j] = 0;
                    }
                    else if (!i)
                    {
                        matrixVector[i][j]       = matrixVector[i][j-1];
                        pointerTableVector[i][j] = S1;
                    }
                    else
                    {
                        matrixVector[i][j]       = matrixVector[i-1][j];
                        pointerTableVector[i][j] = S2;
                    }
                }
            }
        }
    
    public:
        Engine(string s1 , string s2)
        {
            str1       = s1;
            str2       = s2;
            str1Length = (int)str1.length();
            str2Length = (int)str2.length();
            initializeVectors();
        }
    
        void createMatrixForLCSCalculation()
        {
            for(int i = 0 ; i < str1Length ; i++)
            {
                for(int j = 0 ; j < str2Length ; j++)
                {
                    replaceVectorValue(i , j);
                }
            }
        }
    
        string findLCS()
        {
            int i = str1Length - 1;
            int j = str2Length - 1;
            string finalString = "";
            while (i >= 0 && j >= 0)
            {
                if(pointerTableVector[i][j] == S1S2)
                {
                    finalString = str1[i] + finalString;
                    i--;
                    j--;
                }
                else if(pointerTableVector[i][j] == S1)
                {
                    j--;
                }
                else
                {
                    i--;
                }
            }
            return finalString;
        }
};

int main(int argc, const char * argv[])
{
    string str1 = "ACBEA";
    string str2 = "ADCA";
    Engine e    = Engine(str2 , str1);
    e.createMatrixForLCSCalculation();
    cout<<e.findLCS()<<endl;
    return 0;
}
